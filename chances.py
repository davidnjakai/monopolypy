chances_dict = {
    'chances':[
        {
            'description': 'Advance to "Go". (Collect $200)'
        },
        {
            'description': 'Advance to Trafalgar Square. If you pass Go, collect $200.'
        },
        {
            'description': 'Advance to Pall Mall. If you pass Go, collect $200.'
        },
        {
            'description': 'Advance token to nearest Utility. If unowned, you may buy it from the Bank. If owned, throw dice and pay owner a total 10 times the amount thrown.'
        },
        {
            'description': 'Advance token to the nearest Railroad and pay owner twice the rental to which he/she is otherwise entitled. If Railroad is unowned, you may buy it from the Bank.'
        },
        {
            'description': 'Bank pays you dividend of $50.'
        },
        {
            'description': 'Get out of Jail Free. This card may be kept until needed, or traded/sold.'
        },
        {
            'description': 'Go Back Three (3) Spaces.'
        },
        {
            'description': 'Go to Jail. Go directly to Jail. Do not pass GO, do not collect $200.'
        },
        {
            'description': 'Make general repairs on all your property: For each house pay $25, For each hotel pay $100.'
        },
        {
            'description': 'Pay poor tax of $15.'
        },
        {
            'description': 'Take a trip to Kings Cross Station. If you pass Go, collect $200.'
        },
        {
            'description': 'Advance token to Mayfair.'
        },
        {
            'description': 'You have been elected Chairman of the Board. Pay each player $50.'
        },
        {
            'description': 'Your building loan matures. Receive $150.'
        },
        {
            'description': 'You have won a crossword competition. Collect $100.'
        }
    ]
}
