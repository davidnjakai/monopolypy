spaces_dict = {
  'spaces': [
    {
        'name': 'Go',
        'type': 'go',
        'value': 200,
        'player_space': (693, 750, 104, 10),
        'house_space': None,
        'space_orientation': 'CORNER'
    },
    {
      'name': 'Old Kent Road',
      'type': 'property',
      'sub_type': 'brown',
      'value': 60,
      'player_space': (627, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Community Chest',
      'type': 'community_chest',
      'player_space': (562, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Whitechapel Road',
      'type': 'property',
      'sub_type': 'brown',
      'value': 60,
      'player_space': (497, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Income Tax',
      'type': 'tax',
      'sub_type': 'income_tax',
      'value': 200,
      'player_space': (432, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Kings Cross Station',
      'type': 'property',
      'sub_type': 'station',
      'value': 200,
      'player_space': (367, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'The Angel Islington',
      'type': 'property',
      'sub_type': 'turquoise',
      'value': 100,
      'player_space': (302, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Chance',
      'type': 'chance',
      'player_space': (237, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Euston Road',
      'type': 'property',
      'sub_type': 'turquoise',
      'value': 100,
      'player_space': (172, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'Pentonville Road',
      'type': 'property',
      'sub_type': 'turquoise',
      'value': 120,
      'player_space': (107, 750, 64, 10),
      'house_space': None,
      'space_orientation': 'BOTTOM'
    },
    {
      'name': 'In Jail/Just Visiting',
      'type': 'jail_or_visiting',
      'player_space': (0, 750, 104, 10),
      'house_space': None,
      'space_orientation': 'CORNER'
    },
    {
      'name': 'Pall Mall',
      'type': 'property',
      'sub_type': 'pink',
      'value': 140,
      'player_space': (40, 625, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Electric Company',
      'type': 'property',
      'sub_type': 'utility',
      'value': 150,
      'player_space': (40, 560, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Whitehall',
      'type': 'property',
      'sub_type': 'pink',
      'value': 140,
      'player_space': (40, 495, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Northumrld Avenue',
      'type': 'property',
      'sub_type': 'pink',
      'value': 160,
      'player_space': (40, 430, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },

    {
      'name': 'Marylebone Station',
      'type': 'property',
      'sub_type': 'station',
      'value': 200,
      'player_space': (40, 365, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },

    {
      'name': 'Bow Street',
      'type': 'property',
      'sub_type': 'orange',
      'value': 180,
      'player_space': (40, 300, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Community Chest',
      'type': 'community_chest',
      'player_space': (40, 235, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Marlborough Street',
      'type': 'property',
      'sub_type': 'orange',
      'value': 180,
      'player_space': (40, 170, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Vine Street',
      'type': 'property',
      'sub_type': 'orange',
      'value': 200,
      'player_space': (40, 105, 10, 64),
      'house_space': None,
      'space_orientation': 'LEFT'
    },
    {
      'name': 'Free Parking',
      'type': 'free_parking',
      'player_space': (0, 35, 104, 10),
      'house_space': None,
      'space_orientation': 'CORNER'
    },

    {
      'name': 'Strand',
      'type': 'property',
      'sub_type': 'red',
      'value': 220,
      'player_space': (107, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Chance',
      'type': 'chance',
      'player_space': (172, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Fleet Street',
      'type': 'property',
      'sub_type': 'red',
      'value': 220,
      'player_space': (237, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Trafalgar Square',
      'type': 'property',
      'sub_type': 'red',
      'value': 240,
      'player_space': (302, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },

    {
      'name': 'Fenchurch Street Station',
      'type': 'property',
      'sub_type': 'station',
      'value': 200,
      'player_space': (367, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },

    {
      'name': 'Leicester Square',
      'type': 'property',
      'sub_type': 'yellow',
      'value': 260,
      'player_space': (432, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Coventry Street',
      'type': 'property',
      'sub_type': 'yellow',
      'value': 260,
      'player_space': (497, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Water Works',
      'type': 'property',
      'sub_type': 'utility',
      'value': 150,
      'player_space': (562, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Piccadilly',
      'type': 'property',
      'sub_type': 'yellow',
      'value': 280,
      'player_space': (627, 35, 64, 10),
      'house_space': None,
      'space_orientation': 'TOP'
    },
    {
      'name': 'Go To Jail',
      'type': 'go_to_jail',
      'player_space': (693, 35, 104, 10),
      'house_space': None,
      'space_orientation': 'CORNER'
    },
    {
      'name': 'Regent Street',
      'type': 'property',
      'sub_type': 'green',
      'value': 300,
      'player_space': (755, 105, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Oxford Street',
      'type': 'property',
      'sub_type': 'green',
      'value': 300,
      'player_space': (755, 170, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Community Chest',
      'type': 'community_chest',
      'player_space': (755, 235, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Bond Street',
      'type': 'property',
      'sub_type': 'green',
      'value': 320,
      'player_space': (755, 300, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Liverpool Street Station',
      'type': 'property',
      'sub_type': 'station',
      'value': 200,
      'player_space': (755, 365, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Chance',
      'type': 'chance',
      'player_space': (755, 430, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Park Lane',
      'type': 'property',
      'sub_type': 'blue',
      'value': 350,
      'player_space': (755, 495, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Super Tax',
      'type': 'tax',
      'sub_type': 'super_tax',
      'value': 100,
      'player_space': (755, 560, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    },
    {
      'name': 'Mayfair',
      'type': 'property',
      'sub_type': 'blue',
      'value': 400,
      'player_space': (755, 625, 10, 64),
      'house_space': None,
      'space_orientation': 'RIGHT'
    }
  ]
}
