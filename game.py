import random
from spaces import spaces_dict
from chances import chances_dict
from community_chests import community_chests_dict

import pygame

def get_dice_roll():
    return random.randint(1, 6)

class Money(object):
    """This class defines money as objects having a given value"""
    def __init__(self, value):
        self.value = value

class Player(object):
    def __init__(self, player_name, token_image, money, space_position, color):
        self.player_name = player_name
        self.token_image = token_image
        self.money = money
        self.space_position = space_position

        self.in_jail = False
        self.out_of_jail_cards = []
        self.double_counter = 0
        self.extra_turn = False
        self.color = color

    def roll_dice(self):
        return (get_dice_roll(), get_dice_roll())

    def play(self, board):
        action = input("Enter some character to play:\n")
        dice_roll = self.roll_dice()

        print('Player has rolled a {die_1} and a {die_2}\n'.format(die_1=dice_roll[0], die_2=dice_roll[1]))

        doubles = dice_roll[0] == dice_roll[1]

        if doubles:
            if not self.in_jail:
                self.double_counter += 1
                self.extra_turn = True

                #take to jail for 3 consecutive doubles
                if self.double_counter == 3:
                    new_occupied_space = board.space_list[10]
                    self.transfer_player(new_occupied_space, board.space_list, False)
                    self.double_counter = 0
                    self.extra_turn = False
                    self.in_jail = True
                    print('{player_name} has been sent to jail\n'.format(player_name=self.player_name))

            else:
                self.in_jail = False
                print('{player_name} has been freed from jail\n'.format(player_name=self.player_name))

        #restart double counter if not doubles
        else:
            self.double_counter = 0

        spaces_to_move = sum(dice_roll)
        new_occupied_space = self.get_new_occupied_space(spaces_to_move, board.space_list)

        if not self.in_jail:
            self.transfer_player(new_occupied_space, board.space_list)

            self.act_on_space(new_occupied_space, board)

        if self.extra_turn and not self.in_jail:
            self.extra_turn = False
            print('Player has another turn...\n')
            self.play(board)

        #for player who rolls doubles then lands on go to jail
        else:
            self.extra_turn = False

    def get_new_occupied_space(self, spaces_to_move, space_list):
        new_space_position = self.space_position + spaces_to_move
        if new_space_position >= len(space_list):
            return space_list[new_space_position - len(space_list)]
        else:
            return space_list[new_space_position]


    def transfer_player(self, new_occupied_space, space_list, go_salary=True):
        old_space_position = self.space_position
        old_occupied_space = space_list[old_space_position]
        old_occupied_space.players.remove(self)
        old_occupied_space.vacate_space()

        new_space_position = space_list.index(new_occupied_space)
        self.space_position = new_space_position

        new_occupied_space.players.append(self)

        print('{player_name} has landed on space: {space_name}, type: {space_type}\n'.format(player_name=self.player_name, space_name=new_occupied_space.space_name, space_type=new_occupied_space.space_type))

        #check if player passed through Go and is not heading to jail or backtracking
        if old_space_position > new_space_position and go_salary:
            self.money += 200
            print('{player_name} has been awarded $200 at Go.\n'.format(player_name=self.player_name))

    def act_on_space(self, space, board):
        if space.space_type == 'property':
            if space.owner is None:
                self.buy_or_pass(space)
            elif space.owner != self:
                print('You owe {player_name} rent for {space_name}\n'.format(player_name=space.owner.player_name, space_name=space.space_name))
            else:
                print('Welcome home\n')

        elif space.space_type == 'chance':
            chance = board.chance_list.pop()
            chance.action(self, board)

            #check if player has kept the card
            if chance.return_to_deck:
                board.chance_list.insert(0, chance)

        elif space.space_type == 'community_chest':
            community_chest = board.community_chest_list.pop()
            community_chest.action(self, board)

            #check if player has kept the card
            if community_chest.return_to_deck:
                board.community_chest_list.insert(0, community_chest)

        elif space.space_type == 'go_to_jail':
            new_occupied_space = board.space_list[10]
            self.transfer_player(new_occupied_space, board.space_list, False)

            self.in_jail = True
            print('{player_name} has been sent to jail\n'.format(player_name=self.player_name))

    def buy_or_pass(self, space):
        decision = input('Enter \'buy\' to buy or \'pass\' to pass\n')
        while decision not in ['buy', 'pass']:
            print('Invalid input\n')
            decision = input('Enter \'buy\' to buy or \'pass\' to pass\n')
        if decision == 'buy' and self.money >= space.space_value:
            self.money -= space.space_value
            space.owner = self
            print('{player_name} now owns {space_name}\n'.format(player_name=self.player_name, space_name=space.space_name))
        elif decision == 'buy' and self.money < space.space_value:
            print('You don\'t have enough for this property. You have {money}. Property cost is {space_value}\n'.format(money=self.money, space_value=space.space_value))
        else:
            print('Property foregone.\n')

    def get_current_space(self, space_list):
        return space_list[self.space_position]


class Board(object):
    def __init__(self, space_list, community_chest_list, chance_list, player_dict=None):
        self.space_list = space_list
        self.community_chest_list = community_chest_list
        self.chance_list = chance_list
        self.player_dict = player_dict

class Space(object):
    def __init__(self, space_name, space_type, player_space, space_orientation, space_sub_type=None, space_value=None, owner=None):
        self.space_name = space_name
        self.space_type = space_type
        self.space_sub_type = space_sub_type
        self.space_value = space_value
        self.players = []
        self.owner = owner

        self.player_spaces = [] #rectangles where actual players will land
        self.space_orientation = space_orientation

        x = player_space[0]
        y = player_space[1]

        for num in range(1, 9):
            if self.space_orientation in ['LEFT', 'RIGHT']:
                self.player_spaces.append({'player': None, 'space': (x, y, 10, 8)})
                y += 8
            if self.space_orientation in ['TOP', 'BOTTOM', 'CORNER']:
                self.player_spaces.append({'player': None, 'space': (x, y, 8, 10)})
                x += 8

    def draw(self, win):
        for player in self.players:
            available_space = list(filter(lambda x: x['player'] == None, self.player_spaces))[0]
            pygame.draw.rect(win, player.color, available_space['space'])
            available_space['player'] = player

    def vacate_space(self):
        for player_space in self.player_spaces:
            if player_space['player'] not in self.players:
                player_space['player'] = None

class Property(object):
    def __init__(self, property_name, value, rent, house_cost, hotel_cost, mortgage_value):
        self.property_name = property_name
        self.value = value
        self.rent = rent
        self.house_cost = house_cost
        self.hotel_cost = hotel_cost
        self.mortgage_value = mortgage_value

        self.space = Space('property')
        self.owner = None

class Chance(object):
    def __init__(self, chance_index, description):
        self.chance_index = chance_index
        self.description = description
        self.return_to_deck = True

    def action(self, player, board):
        print('{player_name} has collected a chance: \'{chance_description}\'\n'.format(player_name=player.player_name, chance_description=self.description))

        #advance to Go
        if self.chance_index == 0:
            player.transfer_player(board.space_list[0], board.space_list)

        #advance to Trafalgar Square
        elif self.chance_index == 1:
            new_occupied_space = board.space_list[24]
            player.transfer_player(new_occupied_space, board.space_list)
            player.act_on_space(new_occupied_space, board)

        #advance to Pall Mall
        elif self.chance_index == 2:
            new_occupied_space = board.space_list[11]
            player.transfer_player(new_occupied_space, board.space_list)
            player.act_on_space(new_occupied_space, board)

        #advance to nearest utility then buy or pay fee
        elif self.chance_index == 3:
            old_space_position = player.space_position
            #check nearest utility (12 vs 28)
            if old_space_position > 12 and old_space_position < 28:
                new_occupied_space = board.space_list[28]
            else:
                new_occupied_space = board.space_list[12]

            player.transfer_player(new_occupied_space, board.space_list)

            if new_occupied_space.owner is None:
                player.buy_or_pass(new_occupied_space)
            elif new_occupied_space.owner != player:
                space_owner = new_occupied_space.owner
                print('{player_name} to roll dice and pay {space_owner} 10 times the amount thrown\n'.format(player_name=player.player_name, space_owner=space_owner.player_name))
                dice_roll = player.roll_dice()
                print('{player_name} has rolled a {die_1} and a {die_2}\n'.format(player_name=player.player_name, die_1=dice_roll[0], die_2=dice_roll[1]))
                amount_due = sum(dice_roll) * 10
                player.money -= amount_due
                space_owner.money += amount_due
                print('{player_name} has paid {space_owner} ${amount_due}\n'.format(player_name=player.player_name, space_owner=space_owner.player_name, amount_due=amount_due))
            else:
                print('Welcome home\n')

        #advance to nearest railroad and pay owner twice the rent, or buy
        elif self.chance_index == 4:
            old_space_position = player.space_position

            #check nearest railway station (5 vs 15 vs 25 vs 35)
            if old_space_position > 5 and old_space_position < 15:
                new_occupied_space = board.space_list[15]
            elif old_space_position > 15 and old_space_position < 25:
                new_occupied_space = board.space_list[25]
            elif old_space_position > 25 and old_space_position < 35:
                new_occupied_space = board.space_list[35]
            else:
                new_occupied_space = board.space_list[5]

            player.transfer_player(new_occupied_space, board.space_list)

            if new_occupied_space.owner is None:
                player.buy_or_pass(new_occupied_space)
            elif new_occupied_space.owner != player:
                space_owner = new_occupied_space.owner
                print('{player_name} owes {space_owner} twice the rent for {space_name}\n'.format(player_name=player.player_name, space_owner=space_owner.player_name, space_name=new_occupied_space.space_name))
                # TODO: logic for rent payment (*2)
            else:
                print('Welcome home\n')

        #bank pays you dividend of $50
        elif self.chance_index == 5:
            print('Bank has awarded {player_name} a dividend of $50\n'.format(player_name=player.player_name))
            player.money += 50

        #get out of jail free
        elif self.chance_index == 6:
            self.return_to_deck = False
            player.out_of_jail_cards.append(self)
            print('{player_name} has a get out of jail free card\n'.format(player_name=player.player_name))

        #go back 3 spaces
        elif self.chance_index == 7:
            new_occupied_space = board.space_list[player.space_position - 3]

            #transfer player setting go_salary to False since player is backtracking
            player.transfer_player(new_occupied_space, board.space_list, False)

            player.act_on_space(new_occupied_space, board)

        #go directly to jail
        elif self.chance_index == 8:
            new_occupied_space = board.space_list[10]
            player.transfer_player(new_occupied_space, board.space_list, False)
            player.in_jail = True
            print('{player_name} has been sent to jail\n'.format(player_name=player.player_name))

        #general repairs on property
        elif self.chance_index == 9:
            # TODO: houses and hotels logic
            pass

        #pay poor tax of $15
        elif self.chance_index == 10:
            player.money -= 15

        #advance to Kings Cross Station
        elif self.chance_index == 11:
            new_occupied_space = board.space_list[5]
            player.transfer_player(new_occupied_space, board.space_list)
            player.act_on_space(new_occupied_space, board)

        #advance to Mayfair
        elif self.chance_index == 12:
            new_occupied_space = board.space_list[39]
            player.transfer_player(new_occupied_space, board.space_list)
            player.act_on_space(new_occupied_space, board)

        #pay each player $50
        elif self.chance_index == 13:
            for other_player in board.player_dict.values():
                if other_player != player:
                    player.money -= 50
                    other_player.money += 50
                    print('Paid $50 to {other_player}\n'.format(other_player=other_player.player_name))

        #receive $150
        elif self.chance_index == 14:
            player.money += 150
            print('{player_name} awarded $150\n'.format(player_name=player.player_name))

        #collect $100
        elif self.chance_index == 15:
            player.money += 100
            print('{player_name} awarded $100\n'.format(player_name=player.player_name))

class CommunityChest(object):
    def __init__(self, community_chest_index, description):
        self.community_chest_index = community_chest_index
        self.description = description
        self.return_to_deck = True

    def action(self, player, board):
        print('{player_name} has collected a community chest card: \'{community_chest_description}\'\n'.format(player_name=player.player_name, community_chest_description=self.description))

        #advance to Go
        if self.community_chest_index == 0:
            player.transfer_player(board.space_list[0], board.space_list)

        #Bank error, collect $200
        elif self.community_chest_index == 1:
            player.money += 200
            print('{player_name} awarded $200\n'.format(player_name=player.player_name))

        #Pay $20 doctor's fee
        elif self.community_chest_index == 2:
            player.money -= 20
            print('{player_name} paid $20 doctor\'s fee\n'.format(player_name=player.player_name))

        #get $50 from sale of stock
        elif self.community_chest_index == 3:
            player.money += 50
            print('{player_name} awarded $50 from sale of stock\n'.format(player_name=player.player_name))

        #get out of jail free
        elif self.community_chest_index == 4:
            self.return_to_deck = False
            player.out_of_jail_cards.append(self)
            print('{player_name} has a get out of jail free card\n'.format(player_name=player.player_name))

        #Go to jail
        elif self.community_chest_index == 5:
            new_occupied_space = board.space_list[10]
            player.transfer_player(new_occupied_space, board.space_list, False)
            player.in_jail = True
            print('{player_name} has been sent to jail\n'.format(player_name=player.player_name))

        #collect $50 from each player
        elif self.community_chest_index == 6:
            for other_player in board.player_dict.values():
                if other_player != player:
                    player.money += 50
                    other_player.money -= 50
                    print('Received $50 from {other_player}\n'.format(other_player=other_player.player_name))

        #collect $100
        elif self.community_chest_index == 7:
            player.money += 100
            print('{player_name} awarded $100 for holiday fund\n'.format(player_name=player.player_name))

        #collect $20
        elif self.community_chest_index == 8:
            player.money += 20
            print('{player_name} awarded $20 tax refund\n'.format(player_name=player.player_name))

        #collect $100
        elif self.community_chest_index == 9:
            player.money += 100
            print('{player_name} awarded $100 for life insurance\n'.format(player_name=player.player_name))

        #pay $50 hospital fees
        elif self.community_chest_index == 10:
            player.money -= 50
            print('{player_name} paid $50 hospital fees\n'.format(player_name=player.player_name))

        #pay $50 school fees
        elif self.community_chest_index == 11:
            player.money -= 50
            print('{player_name} paid $50 school fees\n'.format(player_name=player.player_name))

        #collect $25 consultancy fee
        elif self.community_chest_index == 12:
            player.money += 25
            print('{player_name} awarded $25 for consultancy fee\n'.format(player_name=player.player_name))

        #street repairs houses and hotels
        elif self.community_chest_index == 13:
            # TODO: houses and hotels logic
            pass

        #collect $10 beauty contest
        elif self.community_chest_index == 14:
            player.money += 10
            print('{player_name} awarded $10 beauty contest\n'.format(player_name=player.player_name))

        #collect $100 inheritance
        elif self.community_chest_index == 15:
            player.money += 100
            print('{player_name} awarded $100 inheritance\n'.format(player_name=player.player_name))

def redrawGameWindow(bg, win, spaces):
    win.fill((0, 0, 0))
    win.blit(bg, (0, 0))
    for space in spaces:
        space.draw(win)
    pygame.display.update()

def main():
    pygame.init()
    projectiles = []
    bg = pygame.image.load('monopoly_board2.jpg')

    clock = pygame.time.Clock()

    win = pygame.display.set_mode((800, 797))

    pygame.display.set_caption("Monopoly board game with Pygame")

    game_over = False

    number_of_players = 2

    players = {}

    chance_list = []
    chance_index = 0

    community_chest_list = []
    community_chest_index = 0

    space_list = []

    for chance in chances_dict['chances']:
        chance_list.append(Chance(chance_index, chance['description']))
        chance_index += 1

    random.shuffle(chance_list)

    for community_chest in community_chests_dict['community_chests']:
        community_chest_list.append(CommunityChest(community_chest_index, community_chest['description']))
        community_chest_index += 1

    random.shuffle(community_chest_list)

    for space in spaces_dict['spaces']:
        if 'sub_type' not in space:
            sub_type = None
        else:
            sub_type = space['sub_type']
        if 'value' not in space:
            value = None
        else:
            value = space['value']
        space_list.append(Space(space['name'], space['type'], space['player_space'], space['space_orientation'], sub_type, value))

    board = Board(space_list, community_chest_list, chance_list)

    #40 spaces
    for player_index in range(number_of_players):
        player_key = 'player_' + str(player_index + 1)
        players[player_key] = Player(player_key, 'default_image', 1500, 0, (255, 0, 0))
        board.space_list[0].players.append(players[player_key])

    board.player_dict = players

    player_turn = 1
    while not game_over:
        clock.tick(27)
        player = players['player_' + str(player_turn)]
        print('{player_name}\'s turn...\n'.format(player_name=player.player_name))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
        keys = pygame.key.get_pressed()

        redrawGameWindow(bg, win, list(filter(lambda x: x.players != [], board.space_list)))
        player.play(board)
        player_turn += 1
        if player_turn > number_of_players:
            player_turn = 1

        keys = pygame.key.get_pressed()

main()
