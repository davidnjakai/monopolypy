community_chests_dict = {
    'community_chests': [
        {
            'description': 'Advance to "Go". (Collect $200)'
        },
        {
            'description': 'Bank error in your favor. Collect $200.'
        },
        {
            'description': 'Doctor\'s fees. Pay $50.'
        },
        {
            'description': 'From sale of stock you get $50.'
        },
        {
            'description': 'Get Out of Jail Free.'
        },
        {
            'description': 'Go to Jail. Go directly to jail. Do not pass Go, Do not collect $200.'
        },
        {
            'description': 'Grand Opera Night. Collect $50 from every player for opening night seats.'
        },
        {
            'description': 'Holiday Fund matures. Collect $100.'
        },
        {
            'description': 'Income tax refund. Collect $20.'
        },
        {
            'description': 'Life insurance matures – Collect $100'
        },
        {
            'description': 'Hospital Fees. Pay $50.'
        },
        {
            'description': 'School fees. Pay $50.'
        },
        {
            'description': 'Receive $25 consultancy fee.'
        },
        {
            'description': 'You are assessed for street repairs: Pay $40 per house and $115 per hotel you own.'
        },
        {
            'description': 'You have won second prize in a beauty contest. Collect $10.'
        },
        {
            'description': 'You inherit $100.'
        }

    ]
}
